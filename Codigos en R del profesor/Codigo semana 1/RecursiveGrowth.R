# Tomas de Camino Beck
# Curso modelos Matematicos ULEAD
# ejemplo con funcion recursiva
# mapa iterativo progamado recursivamente
r <- 2
n0 <- 1
growth <- function(t) {
  if (t == 0)    return (n0)
  else           return (r * growth(t-1))
}
