#acuarios

# definir una matriz
p=matrix(c(0.368,0,0.632,0.368,0.368,0.264,0.184,0.368,0.448),nrow = 3, byrow = T)

#vector (row vector)
pi = c(0,0,1)

#mult row vector by matrix
pi %*% p

#iterative multipliation
for (i in 1:20) {
  pi=pi %*% p
  print(pi)
}