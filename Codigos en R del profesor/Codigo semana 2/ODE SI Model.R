epi=integrateODE(dS~-a*S*I,dI~a*S*I-b*I,a=0.0026,b=0.5,S=500,I=1,tdur=list(from=0, to=20))
plotFun(epi$S(t)~t,t.lim=range(0,20))
plotFun(epi$I(t)~t,t.lim=range(0,20),add=TRUE,col = "red")
