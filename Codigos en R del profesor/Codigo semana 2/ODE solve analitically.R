plotFun(1-x ~x,x.lim=range(0,1))
soln=integrateODE(dx~1-x,x=0,tdur=list(from=0, to=4))
plotFun(soln$x(t)~t,t.lim=range(0,4))
soln$x(1)
