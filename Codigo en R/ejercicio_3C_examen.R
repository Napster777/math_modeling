player_one <- matrix(c(0.3562, 0.3317, 0.3119,
                   0.3317, 0.3562, 0.3119,
                   0.3119, 0.3562, 0.3317), ncol = 3, nrow = 3)

player_two <- matrix(c(0.3509, 0.3342, 0.3147,
                       0.3342, 0.3509, 0.3147,
                       0.3147, 0.3509, 0.3342), ncol = 3, nrow = 3)

stateSet <- c(1,2,3)

newState <- function(s,pMatrix){
  ns = sample(stateSet, size=1, replace=TRUE, prob=pMatrix[s,])
  return(ns)
}

state = vector("numeric")

state[1]=1
for (i in 2:10) {
  state[i] = newState(state[i-1],player_two)
}

print(state)