data <- AgeHeightData
summary(data)

boot_mean <- function(original_vector, resample_vector){
  mean(original_vector$weight[resample_vector])
}
# Revisamos la distribucion de los datos

boot_results <- boot(data, boot_mean, R = 2000)
print(boot_results)
plot(boot_results)

data_new <- data$weight[!is.na(data$weight)]

# Creamos el bootstraping con samples
index <- sample(data_new, replace = TRUE, size = 68)
index <- list(index) # Convertimos la salida en una lista y luego en un
index <- as.data.frame(index) # Data frame
capture.output(index, file = "weight.csv") # Lo almacenamos en un archivo csv y lo sustituimos
#En el conjunto de datos rellenando los valores