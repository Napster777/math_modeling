#Cobweb diagram for ricker model
f.x<- function(x,r){
  exp(r*(1-(x/K)))*x
}


f.temp<-function(xinit,nstep,r){
  xt<- numeric()
  x<- xinit
  xt[1]<- x
  for(i in 2:nstep){
    y<- f.x(x,r)
    x<- y
    xt[i]<- x
  }
  plot(xt,type="b",xlab="time",ylab="x(t)",
       cex.lab=1.7,cex.axis=1.3,lwd=2)
}

iter<- function(xinit,nstep,r){ #Funcion de las iteraciones para el cobweb
  x<- xinit
  y<- f.x(x,r)
  segments(x,0,x,y,lty=1,lwd=2)
  for(i in 1:nstep){
    points(x,y,pch=19,cex=1.5)
    segments(x,y,y,y,lty=1,lwd=2)
    x<- y
    y<- f.x(x,r)
    segments(x,x,x,y,lty=1,lwd=2)
  }
} 

# Parametros iniciales de la funcion
r<- 1.9
K<- 150
xinit<- 2
nstep<- 18
f.temp(xinit,nstep,r)

#Presentar el diagrama de cobweb
plot(0,0,type="n",xlim=c(0,800),ylim=c(0,260),xlab="x(t)",ylab="x(t+1)",
     cex.lab=1.5,cex.axis=1.2)

#La curva de la funcion principal del modelo de ricker
curve(f.x(x,r),from = 0, to = 1000,lty=10,col="blue",lwd=2,add=T)
iter(xinit,nstep,r)